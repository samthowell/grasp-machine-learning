import numpy as np
from numpy.random import default_rng


class KMeans:
    """Implements the naive k-means algorithm.

    .. note:: The Forgy method is used for initialization of the k means.

    :param n_clusters: Number of clusters.
    :param seed: Random number generator seed.
    """

    def __init__(self, n_clusters, eps=1e-6, max_iter=100, seed=None):
        self.n_clusters = n_clusters
        self.eps = eps
        self.max_iter = max_iter
        self.labels = None
        self.centroids = None
        self.error = []
        self._random = default_rng(seed)

    def fit(self, X, y=None):
        """Fit the arrays in X.

        :param X: Data points to be fitted.
        :param y: Unused, for sklearn compatibility reasons.
        """

        # Initialize by drawing k points from X
        centroids = self._random.choice(X, self.n_clusters, replace=False)

        # While not converged or max iterations not reached
        for i in range(self.max_iter):
            print(f"Iteration: {i}")
            # Create clusters by assigning each point to the closest centroid
            error = 1e-6
            labels = np.argmin(
                np.sum(np.power(X.reshape(len(X), 1, -1) - centroids, 2), axis=2),
                axis=1,
            )

            # Recompute centroid position
            new_centroids = np.array(
                [np.mean(X[labels == label], 0) for label in np.unique(labels)]
            )

            # Error
            error = np.sqrt(np.sum(np.power(new_centroids - centroids, 2)))
            self.error.append(error)
            print(error)

            # Update
            centroids = new_centroids

            if error < self.eps:
                break

        self.labels = labels
        self.centroids = centroids

    def predict(self, X, y=None):
        """Fit the samples and return the labels.

        :param X: Data points to be fitted.
        :param y: Unused, for sklearn compatibility reasons.
        """

        self.fit(X, y)

        return self.labels
