import numpy as np


class DBSCAN:
    """Python implementation of density-based spacial clustering of
    applications with noise (DBSCAN) algorithm.

    :param eps: Maximum distance between points.
    :param min_samples: Minimum number of points for clusters to be considered.

    .. note:
            Reference pseudo code adapted from https://en.wikipedia.org/wiki/DBSCAN
    """

    def __init__(self, eps, min_points):
        self.eps = eps
        self.min_points = min_points
        self.points = None
        self.labels = None

    def _norm(self, X, p):
        """Euclidian norm.

        :param X: Array composed of N points with M features.
        :param p: Point with M features
        :returns: Array of length N with norm.
        """
        return np.power(np.sum(np.power(X - p, 2), axis=1), 0.5)

    def _region_query(self, X, ip):
        """
        Return all points that are within p's neighborhood closer than eps.

        :param X: Array composed of N points with M features.
        :param ip: Index of the point which's neighborhood to query.
        :returns: Array with indices of eps-neighbors of p.
        """
        dist = self._norm(X, self.points[ip])
        return np.argwhere(dist <= self.eps).flatten().tolist()

    def _expand_cluster(self, i, iN, c):
        """Expand a cluster.

        :param i: Index of point to expand arount.
        :param iN: Index of neighbor points.
        :param c: Current class label.
        """
        self.labels[i] = c
        iS = iN + [i]
        for j in iS:
            if self.labels[j] == -1:
                self.labels[j] = c

            if self.labels[j] != 0:
                continue

            self.labels[j] = c
            iN = self._region_query(self.points, j)
            if len(iN) >= self.min_points:
                iS.extend(iN)

    def fit(self, X, y=None):
        """Find clusters in X.

        :param X: Array composed of N points with M features.
        :param y: Unused, ony for compatibility with scikit-learn.
        :returns labels: Array of size N with labels. Label -1 denominates noise.
        """

        self.points = X
        self.labels = np.zeros(len(X), dtype="int")

        c = 0
        for i in range(len(self.points)):
            # Point has been visited before
            if self.labels[i] != 0:
                continue

            # Query neighborhood
            iN = self._region_query(X, i)
            if len(iN) < self.min_points:
                self.labels[i] = -1  # Noise
            else:
                c += 1
                self._expand_cluster(i, iN, c)

    def predict(self, X, y=None):

        self.fit(X, y)

        # Shift by 1 that first label starts at 0
        self.labels[self.labels != -1] -= 1
        return self.labels
