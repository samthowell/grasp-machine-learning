"""
Similarity metrics
"""

import numpy as np


def pearson(x, y):
    x_mean = np.mean(x)
    y_mean = np.mean(y)
    return (
        np.sum((x - x_mean) * (y - y_mean))
        / np.sqrt(np.sum((x - x_mean) ** 2))
        / np.sqrt(np.sum((y - y_mean) ** 2))
    )


def r_squared(y, f):
    y_mean = np.mean(y)
    residual_sum_of_squares = np.sum((y - f) ** 2)
    total_sum_of_squares = np.sum((y - y_mean) ** 2)
    return 1 - residual_sum_of_squares / total_sum_of_squares
