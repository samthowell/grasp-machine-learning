"""
Distance metrics
"""
import numpy as np


def accuracy(X, y, model):
    """Compute the accuracy of the predicted samples.
    :param X: Array with the observations.
    :param y: Array with the labels.
    :param model: Model which was trained. Requires a predict method
    :returns: Accuracy of the model.
    """

    return np.mean([yt == model.predict(Xt) for Xt, yt in zip(X, y)])
