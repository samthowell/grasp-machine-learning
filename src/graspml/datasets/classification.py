from collections import namedtuple

import numpy as np

Dataset = namedtuple("Dataset", field_names=["data", "labels", "name", "features"])


# Source: J.R. Quinlan (1986) Machine Learning 1, 81-106
saturday_morning = Dataset(
    data=np.array(
        [
            ["sunny", "hot", "high", "false"],
            ["sunny", "hot", "high", "true"],
            ["overcast", "hot", "high", "false"],
            ["rain", "mild", "high", "false"],
            ["rain", "cold", "normal", "false"],
            ["rain", "cold", "normal", "true"],
            ["overcast", "cold", "normal", "true"],
            ["sunny", "mild", "high", "false"],
            ["sunny", "cold", "normal", "false"],
            ["rain", "mild", "normal", "false"],
            ["sunny", "mild", "normal", "true"],
            ["overcast", "mild", "high", "true"],
            ["overcast", "hot", "normal", "false"],
            ["rain", "mild", "high", "true"],
        ]
    ),
    labels=np.array([0, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0]),
    name="Saturday mornings dataset in Table 1",
    features=["outlook", "temperature", "humidity", "windy"],
)


def split_equal(x, n):
    """Split x into n nearly equally sized sets."""
    assert x >= n, "x must be larger than n."
    if x % n == 0:
        return [x // n for _ in range(n)]
    else:
        r = n - (x % n)
        return [x // n if i < r else x // n + 1 for i in range(n)]


def rotated_blobs(
    n_samples, centers, cluster_std, transformations=None, labels=None, seed=0
):
    """Create a dataset of n samples composed of blobs with centers and and standard deviation."""
    assert len(centers) == len(
        cluster_std
    ), "Length of center and cluster_std must match."

    n_blobs = len(centers)

    if labels is None:
        labels = range(n_blobs)

    sample_size = split_equal(n_samples, n_blobs)

    X = []
    y = []
    for i, label in zip(range(n_blobs), labels):
        n = sample_size[i]
        c = np.array(centers[i])
        std = cluster_std[i]

        np.random.seed(seed + i)
        d = c + np.random.randn(n, 2) * std

        if transformations:
            transformation = [[0.6, -0.6], [-0.4, 0.8]]
            d = np.dot(d, transformation) + c

        X.append(d)
        y.append(np.ones(n, dtype="int") * label)

    X = np.concatenate(X)
    y = np.concatenate(y)

    idx = np.random.choice(range(n_samples), n_samples, replace=False)
    return X[idx], y[idx]


def uniform_points(samples, angle=45, extent=[1, -1], labels=[-1, 1], seed=0):

    rad = angle / 180.0 * np.pi
    w = [np.cos(rad), np.sin(rad)]

    X = np.random.uniform(*extent, (samples, 2))
    y = [labels[0] if (np.dot(x, w) > 0) else labels[1] for x in X]

    return X, y
