import numpy as np
from progressbar import progressbar

from graspml.utils.math import expit


class PerceptronClassifier:
    """Implements a perceptron for binary classification."""

    def __init__(self, alpha):
        # Learning rate
        self.alpha = alpha

        # Initialize NaivePerceptron with random weights
        self.weights = np.random.randn(2)

    def weighted_sum(self, X, weights):
        return sum([x * w for x, w in zip(X, weights)])

    def activation(self, x):
        return -1 if x < 0 else 1

    def predict(self, X):
        X = np.array(X, dtype="float")

        # Dot product of input and weights
        s = self.weighted_sum(X, self.weights)

        # Apply activation function
        return self.activation(s)

    def gradient(self, X, y, weights):
        """Perceptron criterion."""
        if y * np.dot(X, weights) < 0:
            return -y * X
        else:
            return np.zeros_like(weights)

    def train(self, X, y):
        """Train weights on a single observation X, y."""
        X = np.array(X, dtype="float")

        # Update the weights
        self.weights -= self.alpha * self.gradient(X, y, self.weights)


class MultiLayerPerceptronClassifier:
    """
    Implements a Multilayer Perceptron with one hidden layer for multi-classification.

    Parameters
    ----------
    input : int
        Number of input nodes
    hidden : int
        Number of hidden layer nodes
    output : int
        Number of classes
    epochs : int
        Epochs to iterate over during training
    alpha : float
        Learning rate
    batches : int
        Minibatch sample size
    dropout : float
        Dropout rate for input and hidden layer in the interval [0.0, 1.0]
    seed : int
        Random seed
    """

    def __init__(
        self, n_input, n_hidden, n_output, epochs, alpha, batches, dropout=None, seed=0
    ):
        # Layers
        self.input = n_input
        self.hidden = n_hidden
        self.output = n_output

        # Params
        self.epochs = epochs
        self.alpha = alpha
        self.batches = batches
        self.dropout = dropout
        self.training = None

        # Output
        self.cost = []

        # Initialize weights
        self.random = np.random.default_rng(seed)
        self.w1 = self.random.uniform(-1, 1, (self.hidden, self.input + 1))
        self.w2 = self.random.uniform(-1, 1, (self.output, self.hidden + 1))

    def sigmoid(self, a_h):
        """Sigmoid activation
        `s(x) =  1. / (1. + np.exp(-x))`
        """
        return expit(a_h)

    def d_sigmoid(self, a_h):
        """Derivative sigmoid activation"""
        return self.sigmoid(a_h) * (1 - self.sigmoid(a_h))

    def _add_bias(self, v, how="row"):
        """Add ones for bias to column or row."""
        rows, cols = v.shape
        if how == "row":
            v = np.vstack([np.ones(cols), v])
        elif how == "cols":
            v = np.hstack([np.ones((rows, 1)), v])
        return v

    def cost_function(self, y, y_pred):
        """Mean square error."""
        return np.sum(np.abs(y.T - y_pred))

    def feed_forward(self, X, w1, w2):
        """Calculate forward step"""
        X = self._dropout(X)
        z2 = w1.dot(X.T)
        a2 = self.sigmoid(z2)
        a2 = self._dropout(a2)
        a2 = self._add_bias(a2)
        z3 = w2.dot(a2)
        a3 = self.sigmoid(z3)
        return z2, a2, z3, a3

    def predict(self, X, y=None):
        """Predict class of input."""
        X = self._add_bias(X, "cols")
        _, a2, _, a3 = self.feed_forward(X, self.w1, self.w2)
        self.a2 = a2[1:]
        self.a3 = a3
        return a3.T

    def gradient(self, X, y, a2, a3, w2, z2):
        """Calculate back-propagation gradient."""
        d3 = a3 - y.T
        z2 = self._add_bias(z2, "row")
        d2 = w2.T.dot(d3) * self.d_sigmoid(z2)
        grad2 = d3.dot(a2.T)
        grad1 = d2.dot(X)
        return grad1, grad2

    def _dropout(self, x):
        if self.training and self.dropout is not None:
            return x * np.random.choice(
                [0, 1 / (1 - self.dropout)], x.shape, p=[self.dropout, 1 - self.dropout]
            )
        else:
            return x

    def train(self, X, y):
        """Train the network."""
        self.training = True

        X = self._add_bias(X, "cols")

        grad1_prev = np.zeros(self.w1.shape)
        grad2_prev = np.zeros(self.w2.shape)

        n_samples = len(X)
        n_draws = int(n_samples / self.batches)

        for epoch in range(self.epochs):
            for idx in progressbar(
                np.random.choice(n_samples, size=[n_draws, self.batches]),
                prefix=f"Epoch {epoch}/{self.epochs}: ",
            ):

                # Forward propagation
                z2, a2, z3, a3 = self.feed_forward(X[idx], self.w1, self.w2)

                # Backward propagation
                grad1, grad2 = self.gradient(X[idx], y[idx], a2, a3, self.w2, z2)

                # Update weights
                self.w1 -= self.alpha * grad1[1:] + self.alpha * grad1_prev
                self.w2 -= (
                    self.alpha * grad2 + self.alpha * grad2_prev
                )  # TODO: this is weird, why do we not have to remove the bias?

                grad1_prev = grad1[1:]
                grad2_prev = grad2

                # Store cost function
                self.cost.append(self.cost_function(y[idx], a3))

        return self
