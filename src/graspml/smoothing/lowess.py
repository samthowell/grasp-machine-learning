"""
Lowess smothing algorithm
"""

from array import array

import numpy as np


def bicubic(x):
    """Implements the bicubic function."""
    return np.where(np.abs(x) < 1, (1 - x**2) ** 2, 0)


def tricubic(x: array):
    """Implements the tricubic function."""

    return np.where(np.abs(x) < 1, (1 - np.abs(x) ** 3) ** 3, 0)


def weighted_lstsq(y: array, x: array, deg: int, weights: array, *args, **kwargs):
    """Compute a weighted least quare fit.

    :param y:
            Dependent variable to fit
    :param x:
            Independent variable
    :param deg:
            Degree of the polynomial to be fitted.
    :param weights:
            Fitting weights.
    """
    assert len(x) == len(weights), "x, y and weights must have same shape"

    weights = np.sqrt(weights)
    A = np.array(
        [[w * a**d for d in reversed(range(deg + 1))] for w, a in zip(weights, x)]
    )
    y = y * weights

    return *np.linalg.lstsq(A, y, rcond=None, *args, **kwargs), A


class LOWESS:
    """Implements the Locally Weighted Sample Smoothing filter.

    .. seealso: W. Cleaveland, JASA 74(368), 1979 pp 829/836

    .. note: This is a better version: https://github.com/cerlymarco/tsmoothie/blob/5e95
             0a13efaab818fa4358248d74ca2602169a4a/tsmoothie/utils_func.py#L239
    """

    def __init__(
        self, f: float, deg: int = 1, f_weights: str = "tricubic", iterations: int = 2
    ):
        """Initialize with f and optionally d, W, t.

        :param f:
                Parameter determining how many closest neighbor to consider for the
                weighted least square regression
        :param deg:
                Degree of the polynomial used for least square regression. (Default 1)
        :param f_weights:
                Function used to compute the weights for the regression. (Default
                'tricubic')
        :param iterations:
                Number of iterations to improve the initial fit. (Default 2)
        """

        if 0 < f <= 1:
            self.f = f
        else:
            raise ValueError(f"f must be in the interval (0, 1], got '{f}' instead")

        if (deg >= 0) and isinstance(deg, int):
            self.deg = deg
        else:
            raise ValueError(f"deg must a positive integer, got '{deg}' instead")

        if f_weights == "tricubic":
            self._f_weights = tricubic
        elif f_weights == "bicubic":
            self._f_weights = bicubic
        else:
            raise ValueError(f"'{f_weights}' is not a valid weight function")

        if (iterations > 0) and isinstance(iterations, int):
            self.iterations = iterations
        else:
            raise ValueError(
                f"Number of iterations must be larger than 1, got '{iterations}'."
            )

        self.r = None
        self.n_points = None
        self.smoothed = None
        self.standard_error = None
        self.coeffs = None
        self.X = None
        self.bandwidth = None

    def fit(self, X: array, y: array):
        """Piecewise perform smoothing on all y values.

        :param X:
                Independent variable
        :param y:
                Dependent variable which is smoothened.
        """
        X, y = np.array(X), np.array(y)

        # sklearn consistend shape
        assert (
            len(X.shape) == 2
        ), f"X must be a two dimensional array, got {X.shape} instead."

        # X must be sorted
        X = X.reshape(-1)
        order = np.argsort(X)

        X = X[order]
        y = y[order]

        self.n_points = len(X)
        self.bandwidth = int(round(self.n_points * self.f))
        self.X = X

        self.smoothed = np.zeros(self.n_points, dtype="float")
        self.standard_error = np.zeros(self.n_points, dtype="float")
        self.coeffs = None

        for i in range(0, self.n_points):

            # Initial guess
            norm = np.sort(np.abs(X - X[i]))[self.bandwidth]
            weights = self._f_weights((X - X[i]) / norm)

            # Fit the curve
            coeffs, *_ = weighted_lstsq(y, X, deg=self.deg, weights=weights)
            y_pred = np.polyval(coeffs, X)

            # Iterations to improve guess
            for _ in range(0, self.iterations):

                # Residuals
                e = y - y_pred

                # Update weights
                norm = np.median(np.abs(e))
                delta = self._f_weights(e / 6 / norm)
                weights = delta * weights

                # Fit the curve
                coeffs, *_, A = weighted_lstsq(y, X, deg=self.deg, weights=weights)
                y_pred = np.polyval(coeffs, X)

                # Standard error
                sigma2 = np.sum((A.dot(coeffs) - y) ** 2) / self.n_points
                var = np.sqrt(sigma2 * A[i].dot(np.linalg.inv(A.T.dot(A))).dot(A[i]))

            self.coeffs = coeffs
            self.smoothed[i] = y_pred[i]
            self.standard_error[i] = var

    def predict(self, X: array, y=None) -> array:
        """Return the smoothended values on X

        :param X:
                Points on which to return the smoothened values.
        :param y:
                Only for consistency
        """
        raise NotImplementedError()
