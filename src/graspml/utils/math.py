import numpy as np
from numpy.typing import ArrayLike


def expit(x: ArrayLike) -> np.ndarray:
    """Compute logistic sigmoid function (expit).

    Parameters
    ----------
    x : array-like
        The array to apply the expit function

    Returns
    -------
    scalar or ndarray
        An ndarray of the same shape as x, with evaluated entries.
    """

    return 1 / (1 + np.exp(-x))
