from contextlib import nullcontext as does_not_raise

import pytest

from graspml.tree import Node, apply


@pytest.mark.parametrize(
    "kwargs, raises",
    [
        (dict(feature=0), does_not_raise()),
        (dict(feature=0, is_leaf=True), pytest.raises(ValueError)),
        (dict(is_leaf=True, value=0), does_not_raise()),
        (dict(is_leaf=False, value=0), pytest.raises(ValueError)),
    ],
)
def test_node(kwargs, raises):
    with raises:
        node = Node("name", **kwargs)


@pytest.mark.parametrize(
    "node, children, raises",
    [
        (Node("1", feature=0), [("sunny", Node("2", feature=1))], does_not_raise()),
        (
            Node("1", is_leaf=True, value=0),
            [("sunny", Node("2", feature=1))],
            pytest.raises(AssertionError),
        ),
    ],
)
def test_node_add_child(node, children, raises):
    with raises:
        for edge, child in children:
            node.add_child(edge, child)


@pytest.mark.parametrize(
    "node, data, expected",
    [
        (Node("root", is_leaf=True, value=0), [], 0),
        (
            Node("root", feature=0).add_child(0, Node("child", is_leaf=True, value=0)),
            [0, 2, 3],
            0,
        ),
    ],
)
def test_apply(node, data, expected):
    result = apply(node, data)
    assert result == expected
