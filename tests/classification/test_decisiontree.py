import pytest
from numpy.testing import assert_array_equal

from graspml.classification.decisiontree import I, gain, IterativeDichotomiser3
from graspml.datasets.classification import saturday_morning


def test_I():
    """Gest expected information
    This is the same as the example calculation in the paper.

    .. [1] J.R. Quinlan, "Induction of Decision Trees" Machine Learning, vol. 1,
    pp. 81-106, 1986
    """
    i = I(9, 5)
    i = round(i * 1000) / 1000
    assert i == 0.940, f"{I(9, 5)} != 0.940"


def test_gain():
    """Compute gain.

    This is the same as the example calculation in the paper.

    .. [1] J.R. Quinlan, "Induction of Decision Trees" Machine Learning, vol. 1,
           pp. 81-106, 1986
    """
    data = saturday_morning.data[:, 0]
    labels = saturday_morning.labels

    g = gain(data, labels)
    g = round(g * 1000) / 1000
    assert g == 0.247, f"{g} != 0.247"


@pytest.mark.parametrize(
    "data, labels, expected",
    [(saturday_morning.data, saturday_morning.labels, saturday_morning.labels)],
)
def test_iterativedichotomer3_predict(data, labels, expected):
    dt = IterativeDichotomiser3()
    dt.fit(data, labels)
    y_pred = dt.predict(data)
    assert_array_equal(y_pred, expected)
