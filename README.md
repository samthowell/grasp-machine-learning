# `graspml` - Grasp ML Algorithms

This library aims to explain common machine learning algorithms through simple Python code.

There are a plethora of ML packages with highly optimized algorithms; here code 
is kept simple, readable and reduced to the bare minimum to understand their inner workings.


# Conventions
The interface naming conventions should be consistent across the entire package.
Implementation of algorithms from publications follow the naming convention chosen by 
the respective authors to allow simple comparison. 